# Modeling with UML Class Diagrams

Code is a difficult medium to use to communicate designs to humans. It is useful to have a diagram language that abstracts away the details and allow a human to grasp the essential features of a design.

## Content Learning Objectives

After completing this activity, students should be able to:

- Identify parts of UML class diagrams.
- Connect UML class diagram elements with implementation details in Java code.
- Draw UML class diagrams in Markdown with PlantUML

## Process Skill Goals

During the activity, students should make progress toward:

- Interpreting diagrams and differing representations of the same program. (Information Processing)
- Determining the placement of attributes and operations based on UML class diagram notations. (Critical Thinking)
- Creating UML class diagrams in Markdown with PlantUML (Oral and Written Communication)

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1: UML Class Diagrams

The model shows two different representations of the same program: a UML Class Diagram on the top, and Java code below.

---

```plantuml
skinparam classAttributeIconSize 0
hide circle

class Student {
    -{static} nextIdNumber: int
    -firstName : String
    -lastName : String
    -id : String {readOnly}
    -schedule : Course
    +addCourse(course : Course)
    +removeCourse(course : Course)
    +getPrintableSchedule() : String
}
note left of Student::id
    auto-incremented on creation
end note
    

class Course {
    -dept : String
    -number : int
    -section : int
    -credits : int
}
note left of Course::dept
    exactly 2 letters long
end note

Student --> "schedule 0..6" Course : registered for > 
```

---
<!-- markdownlint-disable MD033 -->
<figcaption><code>Student.java</code></figcaption>
<!-- markdownlint-enable MD033 -->

```java
import java.util.*;

public class Student {

    private static int nextIdNumber;
    private String firstName;
    private String lastName;
    private String id;
    private Collection<Course> schedule;

    public void addCourse(Course course) {
        // body goes here
    }

    public void removeCourse(Course course) {
        // body goes here
    }

    public String getPrintableSchedule() {
        // body goes here
    }

    public Student(String firstName, String lastName) {
        //body goes here
    }
}
```
<!-- markdownlint-disable MD033 -->
<figcaption><code>Course.java</code></figcaption>
<!-- markdownlint-enable MD033 -->

```java
public class Course {

    private String dept;
    private int number;
    private int section;
    private int credits;

    public Course(String dept, int number, int section, int credits) {
        //body goes here
    }

}
```

### Questions (30 min)

1. Classes in the program:
    - How many are there?
    - What are their names?
    - How do you know?
    - How is that represented on the diagram?
2. Properties

    For each of the classes:
    - How many properties (attributes or fields or instance variables) do they have?
    - What are their visibility modifiers?
        - How is that represented on the diagram?
    - Which is static?
        - How is that represented on the diagram?
3. Operations

    For the `Student` class:
    - How many operations (or methods) does it have?
    - What are their visibility modifiers?
        - How is that represented on the diagram?
    - What are the parameters of the operations?
        - What are their types?
    - What are the return types of the operations?
        - How is that represented on the diagram?

    For the `Course` class:
    - Why are there no operations listed in the class diagram?
    - When do you think it important to list them in the class diagram?
4. Association
    - How are the Student class and Course class related to each other?
    - Which attribute, in which class, represents the association?
        - How is that represented on the diagram?
    - How many courses can a student have in their schedule?
        - How is that represented on the diagram?
        - How might that be implemented in the code?
5. "Awareness":
    - Is a Student “aware” of which courses are on their schedule?
    - Is a Course “aware” if which students are in the course?
    - How is that represented on the diagram?
6. Notes:
    - What is the use of the note “exactly 2 letters long”?
        - What does it refer to?
        - How would that be reflected in the code?
    - What is the use of the note "auto-incremented on creation"?
        - What does it refer to?
        - How might that be reflected in the code?
7. [Martin Fowler in UmlMode](https://www.martinfowler.com/bliki/UmlMode.html) says that UML diagrams are either [UML as sketch](https://www.martinfowler.com/bliki/UmlAsSketch.html) or [UML as blueprint](https://www.martinfowler.com/bliki/UmlAsBlueprint.html), with sketching being the more common use. If this is a sketching diagram, make a case for leaving out `nextIdNumber` in the `Student` class in the diagram.

## Model 2: Inheritance and Interfaces in UML

This class diagram is not complete - for example, there are no constructors or mutator methods (except for those that change a schedule.)

```plantuml
skinparam classAttributeIconSize 0
hide circle
hide empty attributes
hide empty methods

abstract class UniversityMember {
    -id : String
    -firstName : String
    -lastName : String
    +getId() : String
    +getFirstName() : String
    +getLastName() : String
}

abstract class Employee extends UniversityMember {
    -extension : String
    +getExtension() : String
}

interface HasCourseSchedule <<Interface>> {
    +{abstract}addCourse(course : Course)
    +{abstract}removeCourse(course : Course)
    +{abstract}getPrintableSchedule() : String
}

class Course {
    -dept : String
    -number : int
    -section : int
    -credits : int
}

class Staff extends Employee {
    -title : String
    +getTitle() : String
}

class Faculty extends Employee implements HasCourseSchedule {
    -rank : String
    -schedule : Course
    +getRank() : String
}

class Student extends UniversityMember implements HasCourseSchedule {
    -class : String
    -schedule : Course
    +getClass() : String
}

Course "0..4" <-up- Faculty
Course "0..6" <-up- Student
```

### Questions (20 min)

1. Faculty Class:
    - What attributes are available (can be accessed in a method) to the code in the Faculty class?
    - Where is each defined?
    - What operations are available in the Faculty class?
    - Where is the body for each written?
2. Concrete classes:
    - Which classes are concrete?
    - How is that represented in the diagram?
3. Abstract Classes:
    - Which are abstract?
    - How is that represented in the diagram? *(Hint: Class name formatting)*
    - Where else in the diagram is that formatting used?
4. Arrows:
    - What is the difference between the three types of arrows in the diagram?
    - What does each represent?
5. Write the Java code for the Faculty class. Fill in bodies of non-abstract methods with the comment `// body goes here`.

## Model 3: UML Class Diagrams in Markdown with PlantUML
<!-- markdownlint-disable MD028 -->
> Paste the diagram from Model 1 here when directed to do so

> Paste the diagram from Model 2 here when directed to do so
<!-- markdownlint-disable MD028 -->
<!-- markdownlint-disable MD024 -->
### Questions (20 min)
<!-- markdownlint-enable MD024 -->

#### Model 1 Diagram

1. Copy the PlantUML diagram from Model 1 and paste it where indicated above.
2. What is used to indicate the beginning of the PlantUML code?
3. What is used to indicate the end of the PlantUML code?
4. Where else do you see the same delimiters in the Markdown?
5. Remove the line `skinparam classAttributeIconSize 0` and observe the change in the diagram. What changed?

    Put the line back.
6. Remove the line `hide circle` and observe the change in the diagram. What changed?

    Put the line back.
7. How does PlantUML figure out the name of the class (and put it in the top portion of the box)?
8. How does PlantUML tell the difference between the properties and operations of the class (and put them in the middle and bottom portions of the box respectively)?
9. Change `left` to `right` in the note for Student and observe the change in the diagram. Did it do what you expected?

    Change it back.
10. Add an additional line to the text of the note and observe the change in the diagram. Did it do what you expected?

    Change it back.
11. Remove `::dept` from the note and observe the change in the diagram. What changed?

    Put it back.
12. With the association arrow between `Student` and `Course`:
    1. Change the direction of the arrow. How did you do it?

        Change it back.
    2. Move the attribute and multiplicity to the other end of the arrow. How did you do it?

        Change it back.
    3. Change the direction of the arrow on `registered for`. How did you do it? Did it matter whether you put it before or after the text? Can you put in two arrows, each pointing different directions?

        Change it all back.
    4. Remove one dash from the arrow. What happened?

        Change it back.
    5. Add one more dash to the arrow. What happened?

        Change it back.

#### Model 2 Diagram

1. Copy the PlantUML diagram from Model 2 and paste it where indicated above.
2. Remove the line `hide empty attributes` and observe the change in the diagram. What changed?

    Put the line back.
3. Remove the line `hide empty methods` and observe the change in the diagram. What changed?

    Put the line back.
4. Remove `extends Employee` from the `Staff` class and observe the change in the diagram. What happened?

    Put it back.
5. Remove `implements HasCourseSchedule` from the `Student` class and observe the change in the diagram. What happened?

    Put it back.
6. Remove `up` from the association line between `Course` and `Faculty` and observe the change in the diagram. What happened?
7. Also remove `up` from the association line between `Course` and `Student` and observe the change in the diagram. What happened?
8. Try some other directions with the arrows.
9. Make the changes necessary to get `Course` between `Student` and `Faculty` (horizontally). Paste the new lines below:

Copyright © 2024 Karl R. Wurst.

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
